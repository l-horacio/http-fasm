format ELF64 executable

include "linux.inc"
include "utils.inc"

PORT equ 1234
MAX_CONN equ 16
REQUEST_CAP equ 1024
TODO_SIZE equ 1024
TODO_MAX equ 1024
TODO_MEM_LEN equ (TODO_SIZE * TODO_MAX)
segment readable executable
entry main

main:
    xor rbp, rbp
    funcall read_file, index_start_filepath, index_start_page, index_start_len
    funcall read_file, index_end_filepath, index_end_page, index_end_len
    write STDOUT, init_msg, init_msg.size

    funcall add_todo, todo_example, todo_example.size
    ; Creating socket
    socket AF_INET, SOCK_STREAM, 0
    cmp rax, 0
    jl .on_error_sock
    mov dword [sockfd], eax
    setsockopt [sockfd], IPPROTO_TCP, TCP_NODELAY, 0x01, 0x04

    ; Binding socket
    mov ax, PORT
    xchg ah, al
    mov word [servaddr.port], ax
    bind [sockfd], servaddr, addr_size
    cmp rax, 0
    jne .on_error_bind

    ; Listening connections
    listen [sockfd], MAX_CONN
    cmp rax, 0
    jne .on_error_listen

    .accept_conn:
    ; Accept client connections
    accept [sockfd], clientaddr, client_addr_size
    cmp rax, 0
    je .on_error_accept
    mov [clientfd], eax

    read [clientfd], request_buf, REQUEST_CAP
    mov [request_len], rax
    write STDOUT, request_buf, [request_len]

    funcall str_cmp, request_buf, get_str, get_str.size
    cmp rax, 1
    je .handle_get

    write [clientfd], response_method, response_method.size
    close [clientfd]
    jmp .accept_conn

    write STDOUT, server_ok, server_ok.size
    close [clientfd]
    close [sockfd]
    exit 0

    on_error sock
    on_error bind
    on_error listen
    on_error accept

.handle_get:
    write [clientfd], response_ok, response_ok.size
    write [clientfd], [index_start_page], [index_start_len]
    call fill_todo_list
    write [clientfd], [index_end_page], [index_end_len]
    close [clientfd]
    jmp .accept_conn

error:
    write STDERR, rsi, rdx
    close [sockfd]
    close [clientfd]
    exit 1

add_todo:
    push rbp
    mov rbp, rsp
    mov r8, rdi
    mov r9, rsi
    mov rax, [todo_len]
    imul rax, TODO_MAX
    lea rdi, [todo_buff + rax]
    mov rcx, r9
    mov rsi, r8
    rep movsb
    mov rax, [todo_len]
    inc rax
    mov [todo_len], rax
    leave
    ret

fill_todo_list:
    push rbp
    mov rbp, rsp
    mov rcx, 0
    sub rsp, 8
    mov qword [rbp - 8], todo_buff
.todo_loop_start:
    cmp rcx, [todo_len]
    jge .todo_loop_end

    write [clientfd], todo_item_start, todo_item_start.size
    write [clientfd], [rbp - 8], TODO_MAX
    write [clientfd], todo_item_end, todo_item_end.size

    add qword [rbp - 8], TODO_MAX
    inc rcx
    jmp .todo_loop_start
.todo_loop_end:
    leave
    ret


read_file:
    push rbp
    mov rbp, rsp
    sub rsp, 36
    mov [rbp - 8], rdi
    mov [rbp - 16], rsi
    mov [rbp - 24], rdx
    open [rbp - 8], O_RDONLY, 0
    mov [rbp - 36], eax

    lseek [rbp - 36], 0, SEEK_END
    mov rdx, [rbp - 24]
    mov [rdx], rax
    mov [rbp - 32], rax
    lseek [rbp - 36], 0, SEEK_SET

    mmap [rbp - 24], PROT_READ
    mov rdx, [rbp - 16]
    mov [rdx], rax
    mov r9, rax
    read [rbp - 36], r9, [rbp - 32]
    close[rbp - 36]
    leave
    ret

segment readable writable
    sockfd dd -1
    servaddr servaddr_in AF_INET, INADDR_ANY, 0
    addr_size = $ - servaddr

    clientaddr servaddr_in 0,0,0
    client_addr_size dd addr_size
    clientfd dd -1
    index_start_len dq -1
    index_start_page dq 0
    index_end_len dq -1
    index_end_page dq 0

    request_len dq -1
    request_buf rb 1024
    request_buf_end = $ - request_buf

    todo_len dq 0
    todo_buff rb TODO_MEM_LEN
segment readable
    init_msg string "Stating server...", 10
    server_ok string "Server initialized", 10
    sock_err_msg string "Error creating socket", 10
    bind_err_msg string "Error binding socket", 10
    listen_err_msg string "Error listening socket", 10
    accept_err_msg string "Error accepting client", 10
    index_start_filepath db "./index.start.html",0
    index_end_filepath db "./index.end.html",0
    get_str string "GET"

    todo_item_start string "<li>"
    todo_item_end string "</li>",13,10

    response_ok string "HTTP/1.1 200 OK",13,10,"Connection: close",13,10,"Content-Type: text/html",13,10,13,10
    response_not_found string "HTTP/1.1 404 Not found",13,10,"Connection: close",13,10,"Content-Type: text/html",13,10,13,10,"<h1>Page not found</h1>"
    response_method string "HTTP/1.1 405 Method Not Allowed",13,10,"Connection: close",13,10,"Content-Type: text/html",13,10,13,10,"<h1>Method not Allowed</h1>"

    todo_example string "Write somethig to do", 0
