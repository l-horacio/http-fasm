SYS_READ        equ 0x00
SYS_WRITE       equ 0x01
SYS_OPEN        equ 0x02
SYS_CLOSE       equ 0x03
SYS_LSEEK       equ 0x08
SYS_MMAP        equ 0x09
SYS_MUNMAP      equ 0x0b
SYS_SOCKET      equ 0x29
SYS_CONNECT     equ 0x2a
SYS_ACCEPT      equ 0x2b
SYS_BIND        equ 0x31
SYS_LISTEN      equ 0x32
SYS_SETSOCKOPT  equ 0x36
SYS_EXIT        equ 0x3c

O_RDONLY equ 0x00
SEEK_SET equ 0x00
SEEK_CUR equ 0x01
SEEK_END equ 0x02

MAP_PRIVATE equ 0x00

PROT_READ equ 0x01
PROT_WRITE equ 0x02
PROT_READWRITE equ 0x02

AF_INET equ 0x02
SOCK_STREAM equ 0x01
IPPROTO_TCP equ 0x06
TCP_NODELAY equ 0x01
INADDR_ANY equ 0x00

STDIN  equ 0x00
STDOUT equ 0x01
STDERR equ 0x02

macro on_error name {
.on_error_#name:
mov rsi, name#_err_msg
mov rdx, name#_err_msg.size
call error
}

macro read fd, buf, count
{
mov rax, SYS_READ
mov edi, fd
mov rsi, buf
mov rdx, count
syscall
}

macro write fd, buf, count
{
mov rax, SYS_WRITE
mov edi, fd
mov rsi, buf
mov rdx, count
syscall
}

macro open pathname, flags, mode
{
mov rax, SYS_OPEN
mov rdi, pathname
mov rsi, flags
mov rdx, mode
syscall
}

macro close fd
{
mov rax, SYS_CLOSE
mov edi, fd
syscall
}

macro exit error_code
{
mov rax, SYS_EXIT
mov rdi, error_code
syscall
}

macro socket domain, type, protocol {
mov rax, SYS_SOCKET
mov rdi, domain
mov rsi, type
mov rdx, protocol
syscall
}

macro setsockopt fd, level, optname, optval, optlen {
mov rax, SYS_SETSOCKOPT
mov edi, fd
mov rsi, level
mov rdx, optname
mov r10, optval
mov r8, optlen
syscall
}

macro bind socket, addr, address_len {
mov rax, SYS_BIND
mov edi, socket
mov rsi, addr
mov rdx, address_len
syscall
}

macro listen socket, queue_len {
mov rax, SYS_LISTEN
mov edi, socket
mov rsi, queue_len
syscall
}

macro accept socket, addr, address_len {
mov rax, SYS_ACCEPT
mov edi, socket
mov rsi, addr
mov rdx, address_len
syscall
}

macro lseek fd, offset, whence {
mov rax, SYS_LSEEK
mov edi, fd
mov rsi, offset
mov rdx, whence
syscall
}

macro mmap len, prot {
mov rax, 9
mov rdi, 0
mov rsi, len
mov rdx, 0x03
mov r10, 0x21
mov r8, -1
mov r9, 0
syscall
}
