struc string [data]
{
    common
    . db data
    .size = $ - .
}

struc servaddr_in family, port, addr
{
    .:
    .family dw family
    .port   dw port
    .addr   dd addr
    .zero   dq 0
}

macro funcall name, [args]
{
common
    _ARGS = 0
forward
    if _ARGS = 0
        mov rdi, args
    else if _ARGS = 1
        mov rsi, args
    else if _ARGS = 2
        mov rdx, args
    else if _ARGS = 3
        mov rcx, args
    else if _ARGS = 4
        mov r8, args
    else if _ARGS = 5
        mov r9, args
    end if
    _ARGS = _ARGS + 1
common
    call name
}

str_cmp:
    mov rcx, rdx
    repe cmpsb
    mov rax, 1
    jz .end
    mov rax, 0
.end:
   ret
